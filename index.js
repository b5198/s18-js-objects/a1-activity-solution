/*function Pokemon(name, level){
	// properties
	this.name = name;
	this.level = level;
	this.health = 3 * level;
	this.attack = level;

	// methods
	this.tackle = function(target){
		
		console.log(this.name + " tackled " + target.name)
		console.log(target.name + "'s health is now reduced to " + (target.health - this.attack));
		target.health - this.attack;
		console.log (target.health)
		if(target.health <= 0){
		console.log(this.name + " fainted.")
	}
}
let pikachu  = new Pokemon("Pikachu" , 16)
let charizard =  new Pokemon("Charizard", 8)
console.log(pikachu)
console.log(charizard)

pikachu.tackle(charizard)
pikachu.tackle(charizard)*/

function Pokemon(name, level){
	// properties
	this.name = name;
	this.level = level;
	this.health = 3 * level;
	this.attack = level;

	// methods
	this.tackle = function(target){
		console.log(this.name + " tackled " + target.name)
		console.log(target.name + "'s health is now reduced to " + (target.health - this.attack));
		target.health = (target.health - this.attack);
		if (target.health <= 0){
			console.log(target.name + " Fainted")
		};
	}
}

let MAgumon  = new Pokemon("Metal Agumon" , 99)
let Sukamon =  new Pokemon("Sukamon", 54)
console.log(MAgumon)
console.log(Sukamon)

MAgumon.tackle(Sukamon)
MAgumon.tackle(Sukamon)